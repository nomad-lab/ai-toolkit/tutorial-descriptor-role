import json
from setuptools import setup, find_packages

with open('metainfo.json') as file:
    metainfo = json.load(file)

setup(
    name='descriptor_role',
    version='1.0',
    author=', '.join(metainfo['authors']),
    author_email=metainfo['email'],
    url=metainfo['url'],
    description=metainfo['title'],
    long_description=metainfo['description'],
    packages=find_packages(),
    install_requires=['numpy', 'pandas', 'matplotlib','scikit-learn','ipywidgets','nglview','ase','jupyter_helpers', 'jupyter_jsmol==2021.3.0'],
)
